Maus - AGameEngine2 (2.0.5)
===========================
[Zur Hauptseite](index.html)  
[Zurück zu Tastatur](Tastatur.html)

----------------------------------------------------------------

Funktionen
----------

* [`getMouseX`](#getmousex)
* [`getMouseY`](#getmousey)
* [`getMouseWindowX`](#getmousewindowx)
* [`getMouseWindowY`](#getmousewindowy)
* [`setMousePos`](#setmousepos)
* [`setMouseWindowPos`](#setmousewindowpos)
* [`mousePressed`](#mousepressed)
* [`wasMousePressed`](#wasmousepressed)
* [`getMouseScrollX`](#getmousescrollx)
* [`getMouseScrollY`](#getmousescrolly)
* [`setMouseVisible`](#setmousevisible)

----------------------------------------------------------------
##### getMouseX #####
```python3
ag.getMouseX() -> int
```
Gibt die x-Koordinate des Pixels auf der [Fenstertextur](Fenster.html#getwindowtexture)
zurück, auf die Maus zeigt.
Dies wird nicht durch [`ag.setWindowScale()`](Fenster.html#setwindowscale) beeinflusst.

----------------------------------------------------------------
##### getMouseY #####
```python3
ag.getMouseY() -> int
```
Gibt die y-Koordinate des Pixels auf der [Fenstertextur](Fenster.html#getwindowtexture)
zurück, auf die Maus zeigt.
Dies wird nicht durch [`ag.setWindowScale()`](Fenster.html#setwindowscale) beeinflusst.

----------------------------------------------------------------
##### getMouseWindowX #####
```python3
ag.getMouseWindowX() -> int
```
Gibt die x-Koordinate der Maus auf dem Fenster zurück.

----------------------------------------------------------------
##### getMouseWindowY #####
```python3
ag.getMouseWindowY() -> int
```
Gibt die y-Koordinate der Maus auf dem Fenster zurück.

----------------------------------------------------------------
##### setMousePos #####
```python3
ag.setMousePos(x, y) -> None
```
Setzt die Mausposition im Fenster so, dass sie auf die gegebene Pixelkoordinate
auf der [Fenstertextur](Fenster.html#getwindowtexture) zeigt.

* `x`:
    - `int`
    - Die x-Koordinate des Pixels auf der Fenstertextur
* `y`:
    - `int`
    - Die y-Koordinate des Pixels auf der Fenstertextur

----------------------------------------------------------------
##### setMouseWindowPos #####
```python3
ag.setMouseWindowPos(x, y) -> None
```
Setzt die Mausposition im Fenster auf die gegebenen Koordinate.

* `x`:
    - `int`
    - Die x-Koordinate der Maus im Fenster
* `y`:
    - `int`
    - Die y-Koordinate der Maus im Fenster

----------------------------------------------------------------
##### mousePressed #####
```python3
ag.mousePressed(button) -> bool
```
Gibt zurück, ob der Mausknopf derzeitig gedrückt ist.

* `button`:
    - `str`
    - Der Mausknopf, der abgefragt werden soll
    - Die drei Knöpfe sind `"left"`, `"middle"` oder `"right"`

----------------------------------------------------------------
##### wasMousePressed #####
```python3
ag.wasMousePressed(button) -> bool
```
Gibt zurück, ob der Mausknopf im Moment der Abfrage runtergedrückt wurde.

* `button`:
    - `str`
    - Der Mausknopf, der abgefragt werden soll
    - Die drei Knöpfe sind `"left"`, `"middle"` oder `"right"`

----------------------------------------------------------------
##### getMouseScrollX #####
```python3
ag.getMouseScrollX() -> int
```
Gibt die derzeitige Mausscrollbewegung in x-Richtung zurück.  
Ist der Wert 0, dann wird nicht gescrollt.  
Meistens bedeutet ein positiver Wert, dass nach rechts gescrollt wird
und ein negativer Wert, dass nach links gescrollt wird.  
Die Richtungen werden vom Betriebssystem eingestellt und könnten daher vertauscht sein.

----------------------------------------------------------------
##### getMouseScrollY #####
```python3
ag.getMouseScrollY() -> int
```
Gibt die derzeitige Mausscrollbewegung in y-Richtung zurück.  
Ist der Wert 0, dann wird nicht gescrollt.  
Meistens bedeutet ein positiver Wert, dass nach oben gescrollt wird
und ein negativer Wert, dass nach unten gescrollt wird.  
Die Richtungen werden vom Betriebssystem eingestellt und könnten daher vertauscht sein.

----------------------------------------------------------------
##### setMouseVisible #####
```python3
ag.setMouseVisible(visible) -> None
```
Macht den Mauszeiger im Fenster sichtbar oder unsichtbar.

* `visible`:
    - `bool`
    - Wenn `True`, wird der Mauszeiger sichtbar gemacht
    - Wenn `False`, wird der Mauszeiger unsichtbar gemacht

----------------------------------------------------------------

[Zurück zu Tastatur](Tastatur.html)  
[Weiter zu Gamepads](Gamepads.html)
