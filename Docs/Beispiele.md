Beispiele - AGameEngine2 (2.0.5)
================================
[Zur Hauptseite](index.html)  
[Zurück zu Musik](Musik.html)

----------------------------------------------------------------

Beispiele
---------

* [Bewegliches Rechteck](#bewegliches-rechteck)
* [AGameEngine2 in TKInter](#agameengine2-in-tkinter)
* Hier kommt irgendwann noch mehr dazu

----------------------------------------------------------------

##### Bewegliches Rechteck #####

Ein Fenster, in dem man ein Rechteck mit den Pfeiltasten steuern kann.
Die FPS werden im Fenstertitel angezeigt.
```python3
import AGameEngine2 as ag

# Fenstereigenschaften
winTitle  = "Hallo Welt!"
winWidth  = 800
winHeight = 600
fps = 60
backgroundColor = [0, 50, 50, 255]

# Fenster erstellen und einrichten
ag.setFPS(fps)
ag.createWindow(winTitle, winWidth, winHeight)
winTex = ag.getWindowTexture()

# Spielereigenschaften
playerX = 0
playerY = 0
playerWidth  = 32
playerHeight = 64
playerSpeed = 3
playerColor = [255, 0, 0, 255]

# Hauptschleife
running = True
while running:
    if ag.shouldWindowClose() or ag.keyPressed("escape"):
        running = False

    # Spieler updaten
    if ag.keyPressed("left"):
        playerX -= playerSpeed
    if ag.keyPressed("right"):
        playerX += playerSpeed
    if ag.keyPressed("up"):
        playerY -= playerSpeed
    if ag.keyPressed("down"):
        playerY += playerSpeed

    # Hintergrund, dann Spieler malen
    ag.fillTexture(winTex, backgroundColor)
    ag.drawRect(winTex, playerColor, playerX, playerY, playerWidth, playerHeight)

    ag.waitFPS()
    ag.updateWindow()
    ag.setWindowTitle(winTitle + " FPS: " + str( ag.getFPS() ) )

# AGameEngine2 ordnungsgemäß beenden
ag.quit()

```

----------------------------------------------------------------

##### AGameEngine2 in TKInter #####

Einbettung von AGameEngine2 in TKInter. Wenn der Knopf `Draw!` gedrückt wird,
erscheint ein zufällig platzierter Kreis.

```python3
import AGameEngine2 as ag
import tkinter as tk
from random import randint

# Fenstereigenschaften
winTitle = "Embedded Window!"
agWidth  = 400
agHeight = 300

# TKInter-Fenster vorbereiten und erstellen
win = tk.Tk() # das Fenster
win.title(winTitle)
# wenn das Fenster geschlossen werden soll, dann setze `running` auf `False`
def close(*someArgumentsWeDontCareAbout): # falls TKInter close() mit Argumenten ausführt, dann fange diese, sodass keine Laufzeitfehler entstehen
    global running
    running = False
win.protocol("WM_DELETE_WINDOW", close)
win.bind("<Escape>", close) # Wenn Escape gedrückt wurde

# Einen Rahmen erstellen, wo AGameEngine2 hinein soll
embedTarget = tk.Frame(win, width=agWidth, height=agHeight)
embedTarget.grid(padx=5, pady=5) # ein bisschen Platz zum Fensterrand lassen

def draw(): # Wenn der Knopf gedrückt wurde:
    r = randint(0, 255)
    g = randint(0, 255)
    b = randint(0, 255)
    x = randint(0, agWidth )
    y = randint(0, agHeight)
    radius = randint(5, 50)
    ag.drawCircle(texture, [r, g, b, 255], x, y, radius)
    ag.updateWindow()

# Knopf erstellen
button = tk.Button(text="Draw!", command=draw)
button.grid(row=1, padx=5, pady=5)

# AGameEngine2 vorbereiten
win.update()
ag.createEmbeddedWindow( embedTarget.winfo_id() )
texture = ag.getWindowTexture()
# Hintergrund schwarz füllen
win.update()
ag.fillTexture(texture, [0, 0, 0, 255] )
ag.updateWindow()

# Programm am laufen lassen
running = True
while running:
    ag.update()
    ag.wait(0.05) # Hauptschleife verlangsamen
    win.update()

ag.quit()
win.quit()
```

----------------------------------------------------------------

[Zur Hauptseite](index.html)  
[Zurück zu Musik](Musik.html)
