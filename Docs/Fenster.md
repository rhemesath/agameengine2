Fenster - AGameEngine2 (2.0.5)
==============================
[Zur Hauptseite](index.html)  
[Zurück zu Allgemein](Allgemein.html)

----------------------------------------------------------------

Funktionen
----------

* [`createWindow`](#createwindow)
* [`createEmbeddedWindow`](#createembeddedwindow)
* [`shouldWindowClose`](#shouldwindowclose)
* [`getWindowTexture`](#getwindowtexture)
* [`updateWindow`](#updatewindow)
* [`setWindowTitle`](#setwindowtitle)
* [`setWindowScale`](#setwindowscale)
* [`setWindowFullscreen`](#setwindowfullscreen)
* [`closeWindow`](#closewindow)

----------------------------------------------------------------
##### createWindow #####
```python3
ag.createWindow(title, width, height) -> None
```
Erstellt das Fenster

* `title`:
    - `str`
    - Gibt den Fenstertitel an
* `width`:
    - `int`
    - Gibt die Breite des Fensterinhalts in Pixel an
* `height`:
    - `int`
    - Gibt die Höhe des Fensterinhalts in Pixel an

Man kann nur ein einziges Fenster erstellen.
Möchte man die Bildschirmbreite und -höhe wissen, kann man dafür
[`ag.getDisplayWidth()`](Bildschirme.html#getdisplaywidth) und
[`ag.getDisplayHeight()`](Bildschirme.html#getdisplayheight) verwenden.

----------------------------------------------------------------
##### createEmbeddedWindow #####
```python3
ag.createEmbeddedWindow(windowID) -> None
```
Erstellt das Fenster, welches in einem Anderen eingebettet ist.

* `windowID`:
    - `int` (zumindest bei TKInter)
    - die Fenster-ID des Fensters, in welches AGameEngine eingebettet werden soll

Nutzung mit TKInter:  
Jedes Widget, welches die Methode `winfo_id()` besitzt, sollte als Einbettungsziel
mit der zurückgegebenen Fenster-ID nutzbar sein.  
**Achtung:** AGameEngine-Eingaben ([Tastatur](Tastatur.html), [Maus](Maus.html), [Gamepad](Gamepads.html)) werden nicht registriert. Man muss zumindest für Maus und Tastatur TKInter verwenden

----------------------------------------------------------------
##### shouldWindowClose #####
```python3
ag.shouldWindowClose() -> bool
```
Gibt `True` zurück, wenn das Fenster geschlossen werden soll, andernfalls `False`.  
Das passiert z.B. beim Drücken des roten X oben rechts vom Fenster.  
`ag.shouldWindowClose()` führt [`ag.update()`](Allgemein.html#update) aus.  
Daher sollte `ag.shouldWindowClose()` immer einmal zu Beginn der Hauptschleife ausgeführt werden.

----------------------------------------------------------------
##### getWindowTexture #####
```python3
ag.getWindowTexture() -> SDL_Texture
```
Gibt die [Textur](Texturen.html#textur) vom Fenster zurück. Die Größe ist von [`ag.createWindow()`](#createwindow) gegeben.  
Es handelt sich um eine normale Textur mit der Ausnahme,
dass wenn [`ag.updateWindow()`](#updatewindow) ausgeführt wird, die Textur in ihrem aktuellen Zustand im Fenster angezeigt wird.

----------------------------------------------------------------
##### updateWindow #####
```python3
ag.updateWindow() -> None
```
Aktualisiert das Fenster und zeigt die [Fenstertextur](#getwindowtexture)
in ihrem aktuellen Zustand.  
Bestenfalls wird `ag.updateWindow()` direkt nach [`ag.waitFPS()`](Zeit.html#waitfps)
ausgeführt, damit die Fensteraktualisierung immer in gleichen Zeitabständen geschieht.  
Das kann ruckeln reduzieren.

----------------------------------------------------------------
##### setWindowTitle #####
```python3
ag.setWindowTitle(title) -> None
```
Setzt den Fenstertitel.

* `title`:
    - `str`
    - Der Titel, den das Fenster haben soll

----------------------------------------------------------------
##### setWindowScale #####
```python3
ag.setWindowScale(scaleX, scaleY) -> None
```
Skaliert das Fenster um die gegebenen Faktoren.

* `scaleX`:
    - `int`
    - Darf nicht kleiner als 1 sein
    - Die neue Fensterbreite ist die Breite der [Fenstertextur](#getwindowtexture) multipliziert mit `scaleX`
* `scaleY`:
    - `int`
    - Darf nicht kleiner als 1 sein
    - Die neue Fensterhöhe ist die Höhe der [Fenstertextur](#getwindowtexture) multipliziert mit `scaleY`

Bei Pixelspielen ist es oft notwendig, das gesamte Spiel zu vergrößern, damit man
auf heutigen Bildschirmen etwas erkennen kann.  
Diese Funktion vergrößert nur das Fenster und skaliert den Inhalt. Die Auflösung der
[Fenstertextur](#getwindowtexture) an sich wird nicht verändert!

Möchte man z.B. das Fenster vier Mal so groß haben, verwendet man einfach
`ag.setWindowScale(4, 4)`.
Wenn das Fenster wieder die Originalgröße haben soll, benutzt man `ag.setWindowScale(1, 1)`

----------------------------------------------------------------
##### setWindowFullscreen #####
```python3
ag.setWindowFullscreen(fullscreen, adjustScale=True) -> None
```
Setzt das Fenster in den Vollbildmodus.

* `fullscreen`:
    - `bool`
    - Wenn `True`, dann wird der Vollbildmodus gestartet, sonst wird er beendet.
* `adjustScale`:
    - `bool`
    - Wenn `True`, wird die Bildschirmgröße vom Betriebssystem so angepasst,
      sodass das Fenster möglichst füllend angezeigt wird. Je nach Betriebssystem werden
      Desktopeffekte ausgeschaltet, um die Leistung zu verbessern.
    - Wenn `False`, wird der Bildschirm mit schwarz gefüllt und
      das Fenster wird ohne Fensterränder gemalt

----------------------------------------------------------------
##### closeWindow #####
```python3
ag.closeWindow() -> None
```
Schließt das Fenster unwiderruflich.  
Wenn diese Funktion noch nicht ausgeführt wurde, dann tut es [`ag.quit()`](Allgemein.html#quit).

----------------------------------------------------------------

[Zurück zu Allgemein](Allgemein.html)  
[Weiter zu Texturen](Texturen.html)
