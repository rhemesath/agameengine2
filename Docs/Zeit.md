Zeit - AGameEngine2 (2.0.5)
===========================
[Zur Hauptseite](index.html)  
[Zurück zu Bildschirme](Bildschirme.html)

----------------------------------------------------------------

Bildaktualisierungsrate (FPS)
-----------------------------

Fast immer läuft das Programm zu schnell. Um den Prozessor zu entlasten
und die Bewegungen gleichmäßig wirken zu lassen, verlangsamt man das Programm
mit regelmäßigen Pausen.  
Die Bildaktualisierungsrate (= frame rate) gibt an, wie viele Bilder pro Sekunde (= frames per second = FPS) angezeigt werden. Je höher die FPS, desto flüssiger wirken die Bewegungen und desto
höher ist die Arbeit für den Prozessor.  
Bildschirme haben meistens 60 FPS. Es lohnt sich nicht, die Aktualisierungsrate des Spiels höher
sein zu lassen, als [die vom Bildschirm](Bildschirme.html#getdisplayfps).

----------------------------------------------------------------

V-Sync
------

Vertikale Synchronisation ist dazu da, das
[Aktualisieren des Fensterbildes](Fenster.html#updatewindow) und das
Aktualisieren des Bildschirms im gleichen Takt zu halten. Je nach Betriebssystem und Einstellungen
ist diese Synchronisation erforderlich, um "Screen Tearing" zu vermeiden. Screen Tearing erkennt man
an einer Stelle auf dem Bildschirm, wo eine horizontale Linie das Bild verzerrt. Das entsteht dann, wenn der Bildschirm aus dem Grafikpuffer das Bild gesendet bekommt, aber der Grafikpuffer
das Bild noch nicht vollständig neugezeichnet hat. Also hat man oben auf dem Bildschirm einen Teil des aktuellen Frames und darunter noch den Vorherigen.

----------------------------------------------------------------

Funktionen
----------

* [`setFPS`](#setfps)
* [`waitFPS`](#waitfps)
* [`getFPS`](#getfps)
* [`wait`](#wait)
* [`getTime`](#gettime)

----------------------------------------------------------------
##### setFPS #####
```python3
ag.setFPS(fps) -> None
```
Setzt die erwünschte [Bildaktualisierungsrate](#bildaktualisierungsrate-fps)

* `fps`:
    - `int`
    - Die erwünschte Anzahl der Bilder pro Sekunde

Wird diese Funktion vor [`ag.createWindow()`](Fenster.html#createwindow) ausgeführt
und die [Aktualisierungsrate des Bildschirms](Bildschirme.html#getdisplayfps) gleich `fps` ist, dann wird [V-Sync](#v-sync) verwendet.
Wenn man dies tut, muss sichergestellt werden, dass [`ag.updateWindow()`](Fenster.html#updatewindow)
in der Hauptschleife vorkommt.  
Möchte man V-Sync verhindern, muss `ag.setFPS()` einfach nach [`ag.createWindow()`](Fenster.html#createwindow) gesetzt werden.

----------------------------------------------------------------
##### waitFPS #####
```python3
ag.waitFPS() -> None
```
Pausiert das Programm solange, dass die erwünschten [FPS](#bildaktualisierungsrate-fps) erreicht werden.  
Die erwünschten FPS setzt man [`ag.setFPS()`](#setfps).  

Um ruckeln zu vermeiden, sollte [`ag.updateWindow()`](Fenster.html#updatewindow) direkt nach dieser
Funktion ausgeführt werden.

----------------------------------------------------------------
##### getFPS #####
```python3
ag.getFPS() -> int
```
Gibt die tatsächlich erreichten [FPS](#bildaktualisierungsrate-fps) an.

----------------------------------------------------------------
##### wait #####
```python3
ag.wait(seconds) -> None
```
Pausiert das Programm für die angegebene Zeit

* `seconds`:
    - `float`
    - Die Zeit in Sekunden, die das Programm pausiert werden soll.

----------------------------------------------------------------
##### getTime #####
```python3
ag.getTime() -> float
```
Gibt die Sekunden an, die das Programm schon läuft.

----------------------------------------------------------------

[Zurück zu Bildschirme](Bildschirme.html)  
[Weiter zu Tastatur](Tastatur.html)
