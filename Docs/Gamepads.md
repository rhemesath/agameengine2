Gamepads - AGameEngine2 (2.0.5)
===============================
[Zur Hauptseite](index.html)  
[Zurück zu Maus](Maus.html)

----------------------------------------------------------------

Gamepads
--------

Gamepads sind Spielecontroller. Sie können z.B. über USB oder Bluetooth
angeschlossen werden.

Verschiedene Betriebssysteme mit ihren installierten Treibern verarbeiten die Gamepaddaten
unterschiedlich. Deswegen kann man nie ganz sicher sein, dass Eingaben auf einem Gerät
die gleichen auf einem Anderen sind.  
Unpassende Treiber können folgende Effekte haben:

- Es werden zu viele Knöpfe oder Achsen erkannt, was aber einfach ignoriert werden kann.
- Analogsticks werden nicht als Achsen erkannt, sondern als Steuerkreuze.
- Bestimmte Eingaben werden nicht erkannt.

----------------------------------------------------------------

Funktionen
----------

* [`getNumGamepads`](#getnumgamepads)
* [`getGamepadName`](#getgamepadname)
* [`useGamepad`](#usegamepad)
* [`getNumGamepadButtons`](#getnumgamepadbuttons)
* [`getNumGamepadAxes`](#getnumgamepadaxes)
* [`getNumGamepadHats`](#getnumgamepadhats)
* [`getGamepadButton`](#getgamepadbutton)
* [`getGamepadAxis`](#getgamepadaxis)
* [`getGamepadHatX`](#getgamepadhatx)
* [`getGamepadHatY`](#getgamepadhaty)

----------------------------------------------------------------
##### getNumGamepads #####
```python3
ag.getNumGamepads() -> int
```
Gibt zurück, wie viele Gamepads mit dem Gerät verbunden sind.

----------------------------------------------------------------
##### getGamepadName #####
```python3
ag.getGamepadName(gamepadIndex) -> str
```
Gibt den Namen eines Gamepads zurück.

* `gamepadIndex`:
    - `int`
    - Die Nummer des Gamepads, das untersucht werden soll
    - Das erste Gamepad hat den Index 0

Der Gamepadname kann abgefragt werden, ohne dass das Gamepad mit
[`ag.useGamepad()`](#usegamepad) nutzbar gemacht wurde.

----------------------------------------------------------------
##### useGamepad #####
```python3
ag.useGamepad(gamepadIndex) -> None
```
Bereitet ein Gamepad vor, sodass man davon Eingaben erhalten kann.

* `gamepadIndex`:
    - `int`
    - Die Nummer des Gamepads, das verwendet werden soll
    - Das erste Gamepad hat den Index 0

----------------------------------------------------------------
##### getNumGamepadButtons #####
```python3
ag.getNumGamepadButtons(gamepadIndex) -> int
```
Gibt die Anzahl der Knöpfe eines Gamepads zurück.

* `gamepadIndex`:
    - `int`
    - Die Nummer des Gamepads, das untersucht werden soll
    - Das erste Gamepad hat den Index 0

Das Gamepad darf nur abgefragt werden, wenn es mit [`ag.useGamepad()`](#usegamepad)
nutzbar gemacht wurde.  
Schulterknöpfe, die sich langsam eindrücken lassen (Trigger), werden als Achse erkannt.  

----------------------------------------------------------------
##### getNumGamepadAxes #####
```python3
ag.getNumGamepadAxes(gamepadIndex) -> int
```
Gibt die Anzahl der Achsen eines Gamepads zurück.

* `gamepadIndex`:
    - `int`
    - Die Nummer des Gamepads, das untersucht werden soll
    - Das erste Gamepad hat den Index 0

Ein Analogstick hat zwei Achsen (x- und y-Achse).  
Schulterknöpfe, die sich langsam eindrücken lassen (Trigger), werden als Achse erkannt.  
Das Gamepad darf nur abgefragt werden, wenn es mit [`ag.useGamepad()`](#usegamepad)
nutzbar gemacht wurde.

----------------------------------------------------------------
##### getNumGamepadHats #####
```python3
ag.getNumGamepadHats(gamepadIndex) -> int
```
Gibt die Anzahl der Steuerkreuze eines Gamepads zurück.

* `gamepadIndex`:
    - `int`
    - Die Nummer des Gamepads, das untersucht werden soll
    - Das erste Gamepad hat den Index 0

Das Gamepad darf nur abgefragt werden, wenn es mit [`ag.useGamepad()`](#usegamepad)
nutzbar gemacht wurde.

----------------------------------------------------------------
##### getGamepadButton #####
```python3
ag.getGamepadButton(gamepadIndex, buttonIndex) -> bool
```
Gibt zurück, ob ein Knopf eines Gamepads derzeitig runtergedrückt ist.

* `gamepadIndex`:
    - `int`
    - Die Nummer des Gamepads, das untersucht werden soll
    - Das erste Gamepad hat den Index 0
* `buttonIndex`:
    - `int`
    - Die Nummer des Knopfes auf dem Gamepad, welcher abgefragt werden soll
    - Der erste Knopf hat den Index 0

Das Gamepad darf nur abgefragt werden, wenn es mit [`ag.useGamepad()`](#usegamepad)
nutzbar gemacht wurde.

----------------------------------------------------------------
##### getGamepadAxis #####
```python3
ag.getGamepadAxis(gamepadIndex, axisIndex) -> float
```
Gibt den Wert einer Achse eines Gamepads zurück.

* `gamepadIndex`:
    - `int`
    - Die Nummer des Gamepads, das untersucht werden soll
    - Das erste Gamepad hat den Index 0
* `axisIndex`:
    - `int`
    - Die Nummer der Achse auf dem Gamepad, welche abgefragt werden soll
    - Die erste Achse hat den Index 0

Der Wert einer Achse geht maximal von -1.0 bis 1.0 .  
Schulterknöpfe, die sich langsam eindrücken lassen (Trigger), werden als Achse erkannt.  
Das Gamepad darf nur abgefragt werden, wenn es mit [`ag.useGamepad()`](#usegamepad)
nutzbar gemacht wurde.

----------------------------------------------------------------
##### getGamepadHatX #####
```python3
ag.getGamepadHatX(gamepadIndex, hatIndex) -> int
```
Gibt die x-Achsenrichtung eines Steuerkreuzes zurück.

* `gamepadIndex`:
    - `int`
    - Die Nummer des Gamepads, das untersucht werden soll
    - Das erste Gamepad hat den Index 0
* `hatIndex`:
    - `int`
    - Die Nummer des Steuerkreuzes auf dem Gamepad, welches abgefragt werden soll
    - Das erste Steuerkreuz hat den Index 0

Wenn die Richtung 0 ist, wird das Steuerkreuz in x-Richtung nicht gedrückt.  
Das Steuerkreuz wird nach links gedrückt, wenn die Richtung -1 ist.  
Wenn die Richtung 1 ist, wird nach rechts gedrückt.  
Es kann vorkommen, dass die Richtungen vertauscht sind.  
Das Gamepad darf nur abgefragt werden, wenn es mit [`ag.useGamepad()`](#usegamepad)
nutzbar gemacht wurde.

----------------------------------------------------------------
##### getGamepadHatY #####
```python3
ag.getGamepadHatY(gamepadIndex, hatIndex) -> int
```
Gibt die y-Achsenrichtung eines Steuerkreuzes zurück.

* `gamepadIndex`:
    - `int`
    - Die Nummer des Gamepads, das untersucht werden soll
    - Das erste Gamepad hat den Index 0
* `hatIndex`:
    - `int`
    - Die Nummer des Steuerkreuzes auf dem Gamepad, welches abgefragt werden soll
    - Das erste Steuerkreuz hat den Index 0

Wenn die Richtung 0 ist, wird das Steuerkreuz in x-Richtung nicht gedrückt.  
Das Steuerkreuz wird nach unten gedrückt, wenn die Richtung -1 ist.  
Wenn die Richtung 1 ist, wird nach oben gedrückt.  
Es kann vorkommen, dass die Richtungen vertauscht sind.  
Das Gamepad darf nur abgefragt werden, wenn es mit [`ag.useGamepad()`](#usegamepad)
nutzbar gemacht wurde.

----------------------------------------------------------------

[Zurück zu Maus](Maus.html)  
[Weiter zu Sounds](Sounds.html)
