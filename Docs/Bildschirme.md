Bildschirme - AGameEngine2 (2.0.5)
==================================
[Zur Hauptseite](index.html)  
[Zurück zu Malen](Malen.html)

----------------------------------------------------------------

Funktionen
----------

* [`getNumDisplays`](#getnumdisplays)
* [`getDisplayWidth`](#getdisplaywidth)
* [`getDisplayHeight`](#getdisplayheight)
* [`getDisplayFPS`](#getdisplayfps)

----------------------------------------------------------------
##### getNumDisplays #####
```python3
ag.getNumDisplays() -> int
```
Gibt die Anzahl der gefundenen Bildschirme zurück.

----------------------------------------------------------------
##### getDisplayWidth #####
```python3
ag.getDisplayWidth(displayIndex=0) -> int
```
Gibt die Breite der Bildschirmauflösung in Pixel zurück.

* `displayIndex`:
    - `int`
    - Der Bildschirm, der untersucht werden soll
    - Welcher Bildschirm welchen Index hat, ist von den Nutzereinstellungen des Betriebssystems abhängig

----------------------------------------------------------------
##### getDisplayHeight #####
```python3
ag.getDisplayHeight(displayIndex=0) -> int
```
Gibt die Höhe der Bildschirmauflösung in Pixel zurück.

* `displayIndex`:
    - `int`
    - Der Bildschirm, der untersucht werden soll
    - Welcher Bildschirm welchen Index hat, ist von den Nutzereinstellungen des Betriebssystems abhängig

----------------------------------------------------------------
##### getDisplayFPS #####
```python3
ag.getDisplayFPS(displayIndex=0) -> int
```
Gibt die [Aktualisierungsrate](Zeit.html#bildaktualisierungsrate-fps) des Bildschirms zurück.

* `displayIndex`:
    - `int`
    - Der Bildschirm, der untersucht werden soll
    - Welcher Bildschirm welchen Index hat, ist von den Nutzereinstellungen des Betriebssystems abhängig

----------------------------------------------------------------

[Zurück zu Malen](Malen.html)  
[Weiter zu Zeit](Zeit.html)
