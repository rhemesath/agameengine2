Allgemein - AGameEngine2 (2.0.5)
================================
[Zur Hauptseite](index.html)

----------------------------------------------------------------

AGameEngine2 lässt sich einfach mit
```python3
import AGameEngine2 as ag
```
importieren. Auf das Modul kann man dann mit `ag` zugreifen.

----------------------------------------------------------------

Optionale Parameter
-------------------

In Python können Funktionen Parameter haben, die man nicht zwingend angeben muss.
Beispiel:
```python3
def male(zielTextur, farbe=[255, 0, 0, 255], umrandung=False):
    ...
```
Hier **muss** man `zielTextur` beim ausführen angeben.  
Wenn man `farbe` nicht angibt, wird der Wert `[255, 0, 0, 255]` genommen.  
Und wenn man `umrandung` nicht angibt, wird für den Parameter `False` gesetzt.  
Also:
```python3
male(windowTexture, [255, 0, 0, 255], False)
```
ist das gleiche wie
```python3
male(windowTexture)
```

Möchte man nur bestimmten optionalen Parametern einen Wert geben, dann nimmt man
die jeweiligen Parameternamen und weist ihnen beim ausführen der Funktion den Wert zu:
```python3
male(windowTexture, umrandung=True)
```
ist das gleiche wie
```python3
male(windowTexture, [255, 0, 0, 255], True)
```

----------------------------------------------------------------

Rückgabewerte
-------------

In der dieser Dokumentation wird man auf folgendes stoßen:
```python3
eineFunktion(parameter1, parameter2) -> None
```
Der Pfeil mit dem Datentypen (`-> None`) gibt an, welcher Datentyp von
der Funktion zurückgegeben wird.  
`None` bedeutet, dass die Funktion nichts zurückgibt.

----------------------------------------------------------------

Dateien und Pfade
-----------------

Manche Befehle erfordern das Laden einer Datei, wie z.B. bei [`ag.loadTexture()`](Texturen.html#loadtexture).  
Wenn die zu ladene Datei nicht im gleichen Ordner ist, wo der Code ausgeführt wird,
muss der Dateipfad angegeben werden. Auch unter Windows sollten Pfade im Unix-Stil
geschrieben werden: `"C:/Users/ich/MeinSpiel/Bilder/spieler.png"`

Auch relative Pfade sind möglich: `"Bilder/spieler.png"`

Das übergeordnete Verzeichnis heißt `".."` .
Möchte man also z.B. eine Datei aus dem vorherigen Ordner beschreiben, benutzt man:
`"../musik.mp3"`

----------------------------------------------------------------

Funktionen
----------

* [`update`](#update)
* [`quit`](#quit)

----------------------------------------------------------------
##### update #####
```python3
ag.update() -> None
```
Aktualisiert einige Dinge von AGameEngine, darunter Tastatur und Maus.  
Im Normalfall muss `ag.update()` nicht ausgeführt werden, da [`ag.shouldWindowClose()`](Fenster.html#shouldwindowclose)
es selber tut.

----------------------------------------------------------------
##### quit #####
```python3
ag.quit() -> None
```
Beendet AGameEngine unwiderruflich. Dieser Befehl sollte immer am Ende des Programms stehen.  
`ag.quit()` führt auch [`ag.closeWindow()`](Fenster.html#closewindow) aus.
Zusätzlich löscht diese Funktion alle [Texturen](Texturen.html), die noch geladen sind.

----------------------------------------------------------------

[Weiter zu Fenster](Fenster.html)
