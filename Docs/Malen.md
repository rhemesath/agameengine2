Malen - AGameEngine2 (2.0.5)
============================
[Zur Hauptseite](index.html)  
[Zurück zu Texturen](Texturen.html)

----------------------------------------------------------------

Koordinatensystem
-----------------

Koordinatensysteme von AGameEngine2 haben den Ursprung (x: 0 | y: 0) links oben.  
Die x-Achse verläuft nach rechts.  
Die y-Achse verläuft nach **unten**.  
Jede Textur hat ihr eigenes Koordinatensystem, wo die Einheit stets Pixel ist und
der jeweilige Ursprung links oben ist.

----------------------------------------------------------------

Farben
------

Farben sind eine Liste mit vier Elementen: `[r, g, b, a]`  
Alle Elemente sind vom Datentyp `int` und gehen von 0 bis einschließlich 255.  

Die Farbe ist die Mischung aus `r`, `g` und `b`, also Rot, Grün und Blau.  
Dabei entspricht 0 keine Farbintensität und 255 volle Intensität.

Das Element `a` steht für Alpha, der Farbdeckkraft.  
Wenn Alpha 0 ist, ist die Farbe komplett durchsichtig. Bei 255 ist die Farbe komplett deckend.

```python3
# Beispiele:
[255,   0,   0, 255] # Rot  
[  0, 255,   0, 255] # Grün  
[  0,   0, 255, 255] # Blau  
[255, 255, 255, 255] # Weiß  
[  0,   0,   0, 255] # Schwarz  
[255, 255,   0, 255] # Gelb  
[  0, 255, 255, 255] # Cyan  
[255,   0, 255, 255] # Magenta  
[255, 150,   0, 128] # Halb deckendes Orange  
```
Es besteht die Möglichkeit Werte zu nehmen, die kleiner als 0 oder größer als 255 sind.  
Wenn ein Wert größer als 255 ist, fängt er wieder bei 0 an.  
Wenn ein Wert kleiner als 0 ist, fängt er wieder bei 255 an.

----------------------------------------------------------------

Parameter
---------

Um sich die Reihenfolge der folgenden [Parameter](Allgemein.html#optionale-parameter) besser zu merken, sollte man an folgende
Reihenfolge denken:  
**Worauf**, **Womit**, **Wohin**, **(Wie)**

**Worauf** gibt an, auf welche [Textur](Texturen.html#textur) draufgemalt wird (Zieltextur).  
**Womit** Die [Farbe](#farben) oder [Textur](Texturen.html#textur), mit der gemalt wird.  
**Wohin** Die Position(en) auf der Zieltextur, wohin gemalt werden soll.  
**(Wie)** Der Rest, der nötig ist, um die Form oder [Textur](Texturen.html#textur) zu malen.

Kurze Begriffserklärungen:  
- dest (destination): Ziel. Beschreibt Dinge, die mit der Zieltextur zu tun haben  
- src (source): Quelle / Ursprung. Beschreibt Dinge, die mit der [Textur](Texturen.html#textur) zu tun haben, die gemalt werden soll

----------------------------------------------------------------

Funktionen
----------

* [`fillTexture`](#filltexture)
* [`drawRect`](#drawrect)
* [`drawPixel`](#drawpixel)
* [`drawTexture`](#drawtexture)
* [`drawTextureMod`](#drawtexturemod)
* [`drawLine`](#drawline)
* [`drawLineAA`](#drawlineaa)
* [`drawCircle`](#drawcircle)
* [`drawEllipse`](#drawellipse)
* [`drawTriangle`](#drawtriangle)
* [`drawPolygon`](#drawpolygon)

----------------------------------------------------------------
##### fillTexture #####
```python3
ag.fillTexture(texture, color, replace=True) -> None
```
Füllt eine [Textur](Texturen.html#textur) mit einer [Farbe](#farben).

* `texture`:
    - `SDL_Texture`
    - Die Zieltextur, wo draufgemalt wird
* `color`:
    - `list`
    - Die Farbe, mit der gemalt wird
* `replace`:
    - `bool`
    - Wenn `True`, werden die Pixelfarben von `texture` durch `color` ersetzt
    - Wenn `False`, wird mit `color` auf `texture` draufgemalt.
      Die Farben werden abhängig von ihren Alpha-Werten gemischt.

----------------------------------------------------------------
##### drawRect #####
```python3
ag.drawRect(texture, color, x, y, width, height, replace=False) -> None
```
Malt ein gefülltes Rechteck auf eine [Textur](Texturen.html#textur)

* `texture`:
    - `SDL_Texture`
    - Die Zieltextur, wo draufgemalt wird
* `color`:
    - `list`
    - Die [Farbe](#farben), mit der gemalt wird
* `x`:
    - `int`
    - Die x-Koordinate auf `texture` der oberen linken Ecke des Rechtecks
* `y`:
    - `int`
    - Die y-Koordinate auf `texture` der oberen linken Ecke des Rechtecks
* `width`:
    - `int`
    - Die Breite des Rechtecks
* `height`:
    - `int`
    - Die Höhe des Rechtecks
* `replace`:
    - `bool`
    - Wenn `True`, werden die Pixelfarben von `texture` durch `color` ersetzt
    - Wenn `False`, wird mit `color` auf `texture` draufgemalt.
      Die Farben werden abhängig von ihren Alpha-Werten gemischt.

----------------------------------------------------------------
##### drawPixel #####
```python3
ag.drawPixel(texture, color, x, y, replace=True) -> None
```
Malt ein Pixel auf eine [Textur](Texturen.html#textur).

* `texture`:
    - `SDL_Texture`
    - Die Zieltextur, wo draufgemalt wird
* `color`:
    - `list`
    - Die [Farbe](#farben), mit der gemalt wird
* `x`:
    - `int`
    - Die x-Koordinate auf `texture` der oberen linken Ecke des Pixels
* `y`:
    - `int`
    - Die y-Koordinate auf `texture` der oberen linken Ecke des Pixels
* `replace`:
    - `bool`
    - Wenn `True`, werden die Pixelfarben von `texture` durch `color` ersetzt
    - Wenn `False`, wird mit `color` auf `texture` draufgemalt.
      Die Farben werden abhängig von ihren Alpha-Werten gemischt.

----------------------------------------------------------------
##### drawTexture #####
```python3
ag.drawTexture(destTex, srcTex, destX, destY, region=None, replace=False) -> None
```
Malt auf eine [Textur](Texturen.html#textur) eine Andere drauf.

* `destTex`:
    - `SDL_Texture`
    - Die Zieltextur, wo draufgemalt wird
* `srcTex`:
    - `SDL_Texture`
    - Die Ursprungstextur, mit der gemalt wird
* `destX`:
    - `int`
    - Die x-Koordinate wo auf `destTex` die obere linke Ecke von `srcTex` bzw. `region` hin soll
* `destY`:
    - `int`
    - Die y-Koordinate wo auf `destTex` die obere linke Ecke von `srcTex` bzw. `region` hin soll
* `region`:
    - `list`
    - Beschreibt einen rechteckigen Ausschnitt von `srcTex`, der gemalt werden soll
    - Wenn `None`, dann wird die gesamte `srcTex` gemalt
    - `[srcX, srcY, srcW, srcH]`:
        * `srcX`:
            - `int`
            - x-Koordinate der oberen linken Ecke des Ausschnitts auf `srcTex`
        * `srcY`:
            - `int`
            - y-Koordinate der oberen linken Ecke des Ausschnitts auf `srcTex`
        * `srcW`:
            - `int`
            - Die Breite des Ausschnitts
        * `srcH`:
            - `int`
            - Die Höhe des Ausschnitts
* `replace`:
    - `bool`
    - Wenn `True`, werden die Pixelfarben von `destTex` durch die von `srcTex` ersetzt
    - Wenn `False`, werden die Pixelfarben abhängig von ihren Alpha-Werten gemischt

----------------------------------------------------------------
##### drawTextureMod #####
```python3
ag.drawTextureMod(destTex, srcTex, destX, destY, region=None, destW=None, destH=None, angle=0.0, center=None, flipX=False, flipY=False, replace=False) -> None
```
Malt auf eine [Textur](Texturen.html#textur) eine modifizierte [Textur](Texturen.html#textur) drauf.

* `destTex`:
    - `SDL_Texture`
    - Die Zieltextur, wo draufgemalt wird
* `srcTex`:
    - `SDL_Texture`
    - Die Ursprungstextur, mit der gemalt wird
* `destX`:
    - `int`
    - Die x-Koordinate wo auf `destTex` die obere linke Ecke von `srcTex` bzw. `region` hin soll
* `destY`:
    - `int`
    - Die y-Koordinate wo auf `destTex` die obere linke Ecke von `srcTex` bzw. `region` hin soll
* `region`:
    - `list`
    - Beschreibt einen rechteckigen Ausschnitt von `srcTex`, der gemalt werden soll
    - Wenn `None`, dann wird die gesamte `srcTex` gemalt
    - `[srcX, srcY, srcW, srcH]`:
        * `srcX`:
            - `int`
            - x-Koordinate der oberen linken Ecke des Ausschnitts auf `srcTex`
        * `srcY`:
            - `int`
            - y-Koordinate der oberen linken Ecke des Ausschnitts auf `srcTex`
        * `srcW`:
            - `int`
            - Die Breite des Ausschnitts
        * `srcH`:
            - `int`
            - Die Höhe des Ausschnitts
* `destW`:
    - `int`
    - Die Breite, die `srcTex`/`region` auf `destTex` haben soll. `srcTex`/`region` wird dafür skaliert
    - `None` lässt die Breite unverändert
* `destH`:
    - `int`
    - Die Höhe, die `srcTex`/`region` auf `destTex` haben soll. `srcTex`/`region` wird dafür skaliert
    - `None` lässt die Höhe unverändert
* `angle`:
    - `float`
    - Der Winkel, in der `srcTex`/`region` gemalt werden soll
    - 0.0 bedeutet keine Drehung
    - Die Einheit ist Grad. Es sind negative Werte und Werte über 360 erlaubt
    - Es wird im Uhrzeigersinn gedreht
* `center`:
    - `list`
    - Gibt den Ankerpunkt an, um den mit `angle` gedreht werden soll
    - `None` nimmt die Mitte von `srcTex`/`region`
    - `[centerX, centerY]`:
        * `centerX`:
            - `int`
            - Die x-Koordinate des Ankerpunktes auf `srcTex`/`region`
        * `centerY`:
            - `int`
            - Die y-Koordinate des Ankerpunktes auf `srcTex`/`region`
* `flipX`:
    - `bool`
    - Wenn `True`, dann male links-rechts-gespiegelt. Andernfalls werden diese Seiten nicht gespiegelt
* `flipY`:
    - `bool`
    - Wenn `True`, dann male oben-unten-gespiegelt. Andernfalls werden diese Seiten nicht gespiegelt
* `replace`:
    - `bool`
    - Wenn `True`, werden die Pixelfarben von `destTex` durch die von `srcTex` ersetzt
    - Wenn `False`, werden die Pixelfarben abhängig von ihren Alpha-Werten gemischt

Alle optionalen Parameter lassen sich miteinander kombinieren.
`srcTex` bleibt durch die Modifizierungen unverändert.

----------------------------------------------------------------
##### drawLine #####
```python3
ag.drawLine(texture, color, x1, y1, x2, y2) -> None
```
Malt eine Linie auf eine [Textur](Texturen.html#textur).

* `texture`:
    - `SDL_Texture`
    - Die Zieltextur, wo draufgemalt wird
* `color`:
    - `list`
    - Die [Farbe](#farben), mit der gemalt wird
* `x1`:
    - `int`
    - Die x-Koordinate auf `texture` vom Startpunkt
* `y1`:
    - `int`
    - Die y-Koordinate auf `texture` vom Startpunkt
* `x2`:
    - `int`
    - Die x-Koordinate auf `texture` vom Endpunkt
* `y2`:
    - `int`
    - Die y-Koordinate auf `texture` vom Endpunkt

----------------------------------------------------------------
##### drawLineAA #####
```python3
ag.drawLineAA(texture, color, x1, y1, x2, y2) -> None
```
Malt eine Linie mit anti-aliasing auf eine [Textur](Texturen.html#textur).

* `texture`:
    - `SDL_Texture`
    - Die Zieltextur, wo draufgemalt wird
* `color`:
    - `list`
    - Die [Farbe](#farben), mit der gemalt wird
* `x1`:
    - `int`
    - Die x-Koordinate auf `texture` vom Startpunkt
* `y1`:
    - `int`
    - Die y-Koordinate auf `texture` vom Startpunkt
* `x2`:
    - `int`
    - Die x-Koordinate auf `texture` vom Endpunkt
* `y2`:
    - `int`
    - Die y-Koordinate auf `texture` vom Endpunkt

----------------------------------------------------------------
##### drawCircle #####
```python3
ag.drawCircle(texture, color, x, y, radius) -> None
```
Malt einen gefüllten Kreis auf eine [Textur](Texturen.html#textur).

* `texture`:
    - `SDL_Texture`
    - Die Zieltextur, wo draufgemalt wird
* `color`:
    - `list`
    - Die [Farbe](#farben), mit der gemalt wird
* `x`:
    - `int`
    - Die x-Koordinate auf `texture` vom Mittelpunkt des Kreises
* `y`:
    - `int`
    - Die y-Koordinate auf `texture` vom Mittelpunkt des Kreises
* `radius`:
    - `int`
    - Der Radius

----------------------------------------------------------------
##### drawEllipse #####
```python3
ag.drawEllipse(texture, color, x, y, radius) -> None
```
Malt eine gefüllte Ellipse auf eine [Textur](Texturen.html#textur).

* `texture`:
    - `SDL_Texture`
    - Die Zieltextur, wo draufgemalt wird
* `color`:
    - `list`
    - Die [Farbe](#farben), mit der gemalt wird
* `x`:
    - `int`
    - Die x-Koordinate auf `texture` vom Mittelpunkt der Ellipse
* `y`:
    - `int`
    - Die y-Koordinate auf `texture` vom Mittelpunkt der Ellipse
* `radiusX`:
    - `int`
    - Der Radius in x-Richtung
* `radiusY`:
    - `int`
    - Der Radius in y-Richtung

----------------------------------------------------------------
##### drawTriangle #####
```python3
ag.drawTriangle(texture, color, x1, y1, x2, y2, x3, y3) -> None
```
Malt ein gefülltes Dreieck auf eine [Textur](Texturen.html#textur).

* `texture`:
    - `SDL_Texture`
    - Die Zieltextur, wo draufgemalt wird
* `color`:
    - `list`
    - Die [Farbe](#farben), mit der gemalt wird
* `x1`:
    - `int`
    - Die x-Koordinate auf `texture` der ersten Ecke
* `y1`:
    - `int`
    - Die y-Koordinate auf `texture` der ersten Ecke
* `x2`:
    - `int`
    - Die x-Koordinate auf `texture` der zweiten Ecke
* `y2`:
    - `int`
    - Die y-Koordinate auf `texture` der zweiten Ecke
* `x3`:
    - `int`
    - Die x-Koordinate auf `texture` der dritten Ecke
* `y3`:
    - `int`
    - Die y-Koordinate auf `texture` der dritten Ecke

----------------------------------------------------------------
##### drawPolygon #####
```python3
ag.drawPolygon(texture, color, vertices) -> None
```
Malt ein gefülltes Vieleck auf eine [Textur](Texturen.html#textur).

* `texture`:
    - `SDL_Texture`
    - Die Zieltextur, wo draufgemalt wird
* `color`:
    - `list`
    - Die [Farbe](#farben), mit der gemalt wird
* `vertices`:
    - `list`
    - Eine Liste aus Positionen(=Vertices), welche die Eckpunkte auf `texture` angeben
    - Es müssen mindestens drei Vertices in `vertices` sein
    - Eine Position(=Vertex) ist eine Liste in der Form `[x, y]`:
        * `x`:
            - `int`
            - Die x-Koordinate des Vertex
        * `y`:
            - `int`
            - Die y-Koordinate des Vertex

----------------------------------------------------------------

[Zurück zu Texturen](Texturen.html)  
[Weiter zu Bildschirme](Bildschirme.html)
