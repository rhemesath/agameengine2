Texturen - AGameEngine2 (2.0.5)
===============================
[Zur Hauptseite](index.html)  
[Zurück zu Fenster](Fenster.html)

----------------------------------------------------------------

Textur
------

Eine Textur ist ein Bild, das von der Grafikkarte verarbeitet wird.  
Gleichzeitig ist eine Textur auch eine Leinwand, wo man andere Texturen oder
Formen draufmalen kann. Jede Textur hat ihr eigenes [Koordinatensystem](Malen.html#koordinatensystem).  
Der Datentyp ist `SDL_Texture`.

----------------------------------------------------------------

Funktionen
----------

* [`createTexture`](#createtexture)
* [`loadTexture`](#loadtexture)
* [`saveTexture`](#savetexture)
* [`destroyTexture`](#destroytexture)
* [`getTextureWidth`](#gettexturewidth)
* [`getTextureHeight`](#gettextureheight)
* [`getTexturePixelColor`](#gettexturepixelcolor)

----------------------------------------------------------------
##### createTexture #####
```python3
ag.createTexture(width, height) -> SDL_Texture
```
Erstellt eine neue Textur und gibt sie zurück.

* `width`:
    - `int`
    - Die Breite der Textur in Pixel
* `height`:
    - `int`
    - Die Höhe der Textur in Pixel

----------------------------------------------------------------
##### loadTexture #####
```python3
ag.loadTexture(fileName) -> SDL_Texture
```
Lädt eine Bilddatei und gibt sie als Textur zurück.

* `fileName`:
    - `str`
    - Der Dateiname der [Bilddatei](Allgemein.html#dateien-und-pfade)

----------------------------------------------------------------

##### saveTexture #####
```python3
ag.saveTexture(texture, fileName) -> None
```
Speichert eine Textur als Bilddatei.
Es wird nur Bitmap (.bmp) als Bildtyp unterstützt.

* `texture`:
    - `SDL_Texture`
    - Die Textur, die gespeichert werden soll
* `fileName`:
    - `str`
    - Der Dateiname der [Bilddatei](Allgemein.html#dateien-und-pfade)
    - Die Dateiendung muss `.bmp` sein

----------------------------------------------------------------
##### destroyTexture #####
```python3
ag.destroyTexture(texture) -> None
```
Löscht eine Textur und gibt den Speicher frei.

* `texture`:
    - `SDL_Texture`
    - Die Textur, die gelöscht werden soll

Alle Texturen werden automatisch von [`ag.quit()`](Allgemein.html#quit) gelöscht.

----------------------------------------------------------------
##### getTextureWidth #####
```python3
ag.getTextureWidth(texture) -> int
```
Gibt die Breite einer Textur zurück.

* `texture`:
    - `SDL_Texture`
    - Die Textur, die untersucht werden soll

----------------------------------------------------------------
##### getTextureHeight #####
```python3
ag.getTextureHeight(texture) -> int
```
Gibt die Höhe einer Textur zurück.

* `texture`:
    - `SDL_Texture`
    - Die Textur, die untersucht werden soll

----------------------------------------------------------------
##### getTexturePixelColor #####
```python3
ag.getTexturePixelColor(texture, x, y) -> list
```
Gibt die [Farbe](Malen.html#farben) eines Pixels einer Textur zurück.

* `texture`:
    - `SDL_Texture`
    - Die Textur, die untersucht werden soll
* `x`:
    - `int`
    - Die x-Koordinate des Pixels auf `texture`
* `y`:
    - `int`
    - Die y-Koordinate des Pixels auf `texture`

Diese Funktion ist auf den meisten Geräten ziemlich langsam!  
Möchte man die [Farbe](Malen.html#farben) eines Pixels setzen,
verwendet man [`ag.drawPixel()`](Malen.html#drawpixel).

----------------------------------------------------------------

[Zurück zu Fenster](Fenster.html)  
[Weiter zu Malen](Malen.html)
