Musik - AGameEngine2 (2.0.5)
============================
[Zur Hauptseite](index.html)  
[Zurück zu Sounds](Sounds.html)

----------------------------------------------------------------

Musik
-----

Möchte man Audiodateien als Musik abspielen, sollte man sie nicht als [Sounds](Sounds.html#sounds)
laden, sondern als Musik. Das hat einige Vorteile:

* Man kann komprimierte Dateien (`.mp3`) laden
* Der Musikkanal hat eine eigene Lautstärke
* Musik kann unabhängig von Sounds gesteuert werden (Pausieren, zurückspulen, etc.)

Die Musik wird nicht komplett geladen, sondern beim abspielen gestreamt.  
Der Datentyp von Musik ist `Mix_Music`.

----------------------------------------------------------------

Funktionen
----------

* [`loadMusic`](#loadmusic)
* [`playMusic`](#playmusic)
* [`rewindMusic`](#rewindmusic)
* [`setMusicPos`](#setmusicpos)
* [`pauseMusic`](#pausemusic)
* [`unpauseMusic`](#unpausemusic)
* [`stopMusic`](#stopmusic)
* [`isMusicPlaying`](#ismusicplaying)
* [`setMusicVolume`](#setmusicvolume)
* [`getMusicVolume`](#getmusicvolume)

----------------------------------------------------------------
##### loadMusic #####
```python3
ag.loadMusic(fileName) -> Mix_Music
```
Lädt eine Audiodatei als [Musik](#musik).

* `fileName`:
    - `str`
    - Der [Dateiname](Allgemein.html#dateien-und-pfade) der Audiodatei.
    - Musikdateien müssen vom Typ `.mp3` sein.

----------------------------------------------------------------
##### playMusic #####
```python3
ag.playMusic(music, loops=0) -> None
```
Spielt eine [geladene Musik](#loadmusic) ab.

* `music`:
    - `Mix_Music`
    - Die Musik, die abgespielt werden soll
* `loops`:
    - `int`
    - Gibt an, wie viele Wiederholungen die Musik machen soll
    - 0 bedeutet keine Wiederholungen.
    - Die Sound wird fortlaufend wiederholt, wenn der Wert -1 ist.


Es kann nur eine Musik gleichzeitig abgespielt werden.

----------------------------------------------------------------
##### rewindMusic #####
```python3
ag.rewindMusic() -> None
```
Setzt die [Musik](#musik) zurück zum Anfang.  
Hat den gleichen Effekt wie [`ag.setMusicPos(0)`](#setmusicpos).

----------------------------------------------------------------
##### setMusicPos #####
```python3
ag.setMusicPos(second) -> None
```
Spielt die [Musik](#musik) ab der angegebener Sekunde weiter.

* `second`:
    - `float`
    - Die Sekunde, zu der gesprungen werden soll
    - Der Anfang der Musik ist 0.0

----------------------------------------------------------------
##### pauseMusic #####
```python3
ag.pauseMusic() -> None
```
Pausiert den Kanal der [Musik](#musik).

----------------------------------------------------------------
##### unpauseMusic #####
```python3
ag.unpauseMusic() -> None
```
Macht eine Pausierung der [Musik](#musik) rückgängig.  
Musik wird mit [`ag.pauseMusic()`](#pausemusic) pausiert.

----------------------------------------------------------------
##### stopMusic #####
```python3
ag.stopMusic() -> None
```
Entfernt die [Musik](#musik) aus dem Musikkanal.

----------------------------------------------------------------
##### isMusicPlaying #####
```python3
ag.isMusicPlaying() -> bool
```
Gibt zurück, ob derzeitig [Musik](#musik) gespielt wird.  
Es wird auch `False` zurückgegeben, wenn der Musikkanal [pausiert](#pausemusic) ist.

----------------------------------------------------------------
##### setMusicVolume #####
```python3
ag.setMusicVolume(volume) -> None
```
Setzt die Lautstärke, mit der [Musik](#musik) abgespielt wird.

* `volume`:
    - `float`
    - Der Wert geht von 0.0 bis 1.0
    - Normale Lautstärke ist 1.0
    - Keine Lautstärke ist 0.0

Standartmäßig ist die Musiklautstärke 1.0 .

----------------------------------------------------------------
##### getMusicVolume #####
```python3
ag.getMusicVolume() -> float
```
Gibt die Lautstärke zurück, mit der die [Musik](#musik) abgespielt wird.  
Der Wert geht von 0.0 bis einschließlich 1.0  
Normale Lautstärke ist 1.0, keine Lautstärke ist 0.0  
Standartmäßig ist die Musiklautstärke 1.0 .

----------------------------------------------------------------

[Zurück zu Sounds](Sounds.html)  
[Weiter zu Beispiele](Beispiele.html)
