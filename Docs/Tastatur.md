Tastatur - AGameEngine2 (2.0.5)
===============================
[Zur Hauptseite](index.html)  
[Zurück zu Zeit](Zeit.hmtl)

----------------------------------------------------------------

Tasten
------

Hier ist eine Liste der Tasten, die man abfragen kann
```python3
# Tasten 0 - 9
"0"
"1"
"2"
"3"
"4"
"5"
"6"
"7"
"8"
"9"

# A - Z
"a"
"b"
"c"
"d"
"e"
"f"
"g"
"h"
"i"
"j"
"k"
"l"
"m"
"n"
"o"
"p"
"q"
"r"
"s"
"t"
"u"
"v"
"w"
"x"
"y"
"z"

# Funktionstasten
"f1"
"f2"
"f3"
"f4"
"f5"
"f6"
"f7"
"f8"
"f9"
"f10"
"f11"
"f12"

# Pfeiltasten
"left"
"right"
"up"
"down"

# Sonstiges
"alt"
"ctrl"      # Strg links
"shift"     # links
"caps"
"tab"
"enter"
"space"     # Leertaste
"backspace"
"escape"

```

----------------------------------------------------------------

Funktionen
----------

* [`keyPressed`](#keypressed)
* [`wasKeyPressed`](#waskeypressed)
* [`getKeyStrings`](#getkeystrings)

----------------------------------------------------------------
##### keyPressed #####
```python3
ag.keyPressed(key) -> bool
```
Gibt zurück, ob die angegebene Taste derzeitig gedrückt ist.

* `key`:
    - `str`
    - Die [Taste](#tasten), die abgefragt werden soll

----------------------------------------------------------------
##### wasKeyPressed #####
```python3
ag.wasKeyPressed(key) -> bool
```
Gibt zurück, ob die angegebene Taste runtergedrückt wurde.

* `key`:
    - `str`
    - Die [Taste](#tasten), die abgefragt werden soll

Es wird nur `True` zurückgegeben, wenn die Taste in dem Moment der Abfrage
runtergedrückt wurde.  
Wird die Taste gehalten, entscheidet das Betriebssystem, was passiert.
Meistens wird eine gewisse Zeit `False` zurückgegeben und dann wiederholend in gleichmäßigen
Intervallen einmal `True`, dann eine kurze Zeit lang `False`. Das Verhalten zeigt sich, wenn man
auf dem Betriebssystem bei einer Texteingabe eine Taste gedrückt hält.

----------------------------------------------------------------
##### getKeyStrings #####
```python3
ag.getKeyStrings() -> list
```
Gibt alle abfragbaren [Tasten](#tasten) zurück.

----------------------------------------------------------------

[Zurück zu Zeit](Zeit.html)  
[Weiter zu Maus](Maus.html)
