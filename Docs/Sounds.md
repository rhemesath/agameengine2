Sounds - AGameEngine2 (2.0.5)
=============================
[Zur Hauptseite](index.html)  
[Zurück zu Gamepads](Gamepads.html)

----------------------------------------------------------------

Sounds
------

Sounds sind kleine Audiodateien, die schnell geladen und abgespielt werden können.
Da sie aber recht viel Arbeitsspeicher benötigen, werden die Dateien nicht vollständig,
sondern nur in kleinen Teilen geladen. Beim abspielen werden die benötigten Daten dann gestreamt.
Der Datentyp eines Sounds ist `Mix_Chunk`.

----------------------------------------------------------------

Funktionen
----------

* [`loadSound`](#loadsound)
* [`loadSoundsFromDir`](#loadsoundsfromdir)
* [`playSound`](#playsound)
* [`setSoundsVolume`](#setsoundsvolume)
* [`getSoundsVolume`](#getsoundsvolume)
* [`areSoundsPlaying`](#aresoundsplaying)

----------------------------------------------------------------
##### loadSound #####
```python3
ag.loadSound(fileName) -> Mix_Chunk
```
Lädt eine Audiodatei als [Sound](#sounds)

* `fileName`:
    - `str`
    - Der [Dateiname](Allgemein.html#dateien-und-pfade) der Audiodatei.
    - Sounds müssen `.wav`-Dateien sein!

----------------------------------------------------------------
##### loadSoundsFromDir #####
```python3
ag.loadSoundsFromDir(directory) -> dict
```
Lädt alle `.wav`-Dateien eines Ordners und gibt sie als [Sounds](#sounds) zurück.

* `directory`:
    - `str`
    - Das [Verzeichnis (= Ordner)](Allgemein.html#dateien-und-pfade),
    aus dem die Sounds geladen werden sollen

Die Keys des Dictionarys sind die Dateinamen ohne Endung.  
Beispiel:  
Im Ordner `GameSounds` seien die Audiodateien `jump.wav` und `shoot.wav`.  
```python3
sounds = ag.loadSoundsFromDir("GameSounds")
jumpSound  = sounds["jump"]
shootSound = sounds["shoot"]
```

----------------------------------------------------------------
##### playSound #####
```python3
ag.playSound(sound, loops=0) -> None
```
Spielt einen [geladenen Sound](#loadsound) ab.

* `sound`:
    - `Mix_Chunk`
    - Der Sound, der abgespielt werden soll
* `loops`:
    - `int`
    - Gibt an, wie viele Wiederholungen der Sound machen soll
    - 0 bedeutet keine Wiederholungen.
    - Der Sound wird fortlaufend wiederholt, wenn der Wert -1 ist.

Es können mehrere Sounds gleichzeitig gespielt werden.

----------------------------------------------------------------
##### setSoundsVolume #####
```python3
ag.setSoundsVolume(volume) -> None
```
Setzt die Lautstärke, mit der [Sounds](#sounds) abgespielt werden.

* `volume`:
    - `float`
    - Der Wert geht von 0.0 bis einschließlich 1.0
    - Normale Lautstärke ist 1.0
    - Keine Lautstärke ist 0.0

Standartmäßig ist die Soundlautstärke 1.0 .  
Das Ändern der Soundlautstärke beeinträchtigt nicht die [Musiklautstärke](Musik.html#setmusicvolume)

----------------------------------------------------------------
##### getSoundsVolume #####
```python3
ag.getSoundsVolume() -> float
```
Gibt die Lautstärke zurück, mit der die [Sounds](#sounds) abgespielt werden.  
Der Wert geht von 0.0 bis einschließlich 1.0  
Normale Lautstärke ist 1.0, keine Lautstärke ist 0.0  
Standartmäßig ist die Soundlautstärke 1.0 .  

----------------------------------------------------------------
##### areSoundsPlaying #####
```python3
ag.areSoundsPlaying() -> bool
```
Gibt zurück, ob derzeitig mindestens ein [Sound](#sounds) abgespielt wird.

----------------------------------------------------------------

[Zurück zu Gamepads](Gamepads.html)  
[Weiter zu Musik](Musik.html)
